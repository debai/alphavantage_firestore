"""
This script transfers data from alphavantage to firestore

"""
import datetime
import requests
from google.cloud import firestore
import time
import hashlib
import os


API_KEY = os.environ["ALPHAVANTAGE_KEY"]
DB = firestore.Client()


def single_stock(stock, json_interval, query, api_key, collection):
    """
    Pulls stock data for a single stock via alphavantage api and loads it into firestore
    :param stock: str
    :param json_interval: str,  json key to look for in the api call
    :param query: str, essentially the api get request
    :param api_key: str
    :param collection: str, the collection in firestore
    :return: None
    """

    batch = DB.batch()
    api_data = requests.get(query + api_key).json()
    num_entries = 0

    for time_stamp in api_data[json_interval]:

        document_data = dict()

        for attribute in api_data[json_interval][time_stamp]:
            # get rid of numbers in the keys and turn values from strings to numbers
            document_data[attribute.split(" ")[-1]] = float(api_data[json_interval][time_stamp][attribute])

        document_data["stock"] = stock
        document_data["date"] = time_stamp[:10]
        document_data["time"] = time_stamp[11:]

        doc_id = hashlib.sha256((stock + " " + time_stamp).encode()).hexdigest()
        ref = DB.collection(collection).document(doc_id)
        batch.set(ref, document_data)
        num_entries += 1

        if num_entries == 500:  # Firestore max writes per batch is 500
            batch.commit()
            num_entries = 0

    batch.commit()  # commit remaining documents


def all_stocks(initial=True):
    """
    Gets the lists of stocks that will be pulled from alphavantage and calls a function to do etl on the stocks
    one by one
    :return: None
    """

    etl_ref = DB.collection(u'etl')
    etl_docs = []  # filled with lists of the form (etl_doc_ref = firestore_ref_string, etl_doc = dict())
    query = None

    if initial:
        # This is the initial attempt
        query = etl_ref.where(u'scheduled_day', u'==', datetime.date.today().weekday()) \
            .where(u'last_attempt', u'<', str(datetime.date.today())[:10]) \
            .stream()
    else:
        # This is the clean up attempt
        query = etl_ref.where(u'last_attempt_comments', u'==', "fail").stream()

    # should technically use list comprehension... but not really a performance issue here.
    for doc in query:
        etl_docs += [[DB.collection(u'etl').document(doc.id), doc.to_dict()]]

    # Performing etl for each stock in etl_docs
    api_request_count = 0
    start_time = time.time()

    for ref_doc in etl_docs:
        print("start stock:", ref_doc[1]["stock"])

        try:
            ref_doc[1]["last_attempt"] = str(datetime.date.today())[:10]
            single_stock(ref_doc[1]["stock"], ref_doc[1]["json_interval"], ref_doc[1]["api_endpoint"], API_KEY,
                         ref_doc[1]["collection"])
            ref_doc[1]["last_attempt_successful"] = str(datetime.date.today())[:10]
            ref_doc[1]["last_attempt_comments"] = "success"
            ref_doc[0].set(ref_doc[1])

            print("finished stock:", ref_doc[1]["stock"])

        except:
            print("fail")

            ref_doc[1]["last_attempt"] = str(datetime.date.today())[:10]
            ref_doc[1]["last_attempt_comments"] = "fail"
            ref_doc[0].set(ref_doc[1])

        api_request_count += 1

        # alphavantage subscription has an api limit of 30 / min
        if api_request_count == 30:
            print("#" * 40)

            if 70 >= time.time() - start_time:  # could technically be 60 secs, but just to be safe, made it 70.
                time.sleep(10)

            api_request_count = 0
            start_time = time.time()


#all_stocks(initial=False)
