from google.cloud import firestore
import pandas as pd


db = firestore.Client()
batch = db.batch()

data = pd.read_csv("etl.csv")
data = data.fillna("")
data = data.to_dict("records")


for i in data:
    print(i)

num_entries = 0
for row in data:

    ref = db.collection("etl").document(row["key"])
    del row["key"]  # don't need key to be an uploaded field

    batch.set(ref, row)
    num_entries += 1

    if num_entries == 500:  # Firestore max writes per batch is 500
        batch.commit()
        num_entries = 0

batch.commit()



