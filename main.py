import alphavantage_firestore
from flask import Flask, request
app = Flask(__name__)


@app.route('/initial', methods=['GET', 'POST'])
def initial():
    alphavantage_firestore.all_stocks(initial=True)
    return 'yolo'


@app.route('/cleanup', methods=['GET', 'POST'])
def cleanup():
    alphavantage_firestore.all_stocks(initial=False)
    return "yes"


if __name__ == '__main__':
    app.run(debug=True)

